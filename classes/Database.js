const utils = require('../utils')
const path = require('path')
const fs = require('fs')
const config = require('../config')
const dbConfig = require('../private/db')
const mysql = require('mysql')

const Database = {
    init: function(){
        this.connection = mysql.createConnection(dbConfig.connectionConfig)
        return this
    },

    query: function(queryString){
        console.log('running query...')
        return new Promise((res, rej)=>{
            this.connection.query(queryString, (err, rows, fields)=>{
                if(err)rej(err)
                res(rows)
            })
        })
    },

    addUser: function(userObj){
        const tableName = 'users'
        const schema = Object.keys(userObj)
        const values = Object.values(userObj).map(obj=>'\''+obj+'\'')
        const queryString = 'INSERT INTO '+tableName+'('+schema+')'+' values('+values+');'
        
        console.log('queryString: '+queryString)

        return this.query(queryString)
    }
}

module.exports = Database