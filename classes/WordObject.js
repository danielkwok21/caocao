const crypto = require('crypto')
const utils = require('../utils')

let WordObject = {
    
    // default constructor for creating WordObject from results.json
    init: function(string, vertices, key=""){
        this.string = string

        this.topLeft = Object.create(Coordinate).init(vertices[0].x, vertices[0].y)
        this.topRight = Object.create(Coordinate).init(vertices[1].x, vertices[1].y)
        this.bottomRight = Object.create(Coordinate).init(vertices[2].x, vertices[2].y)
        this.bottomLeft = Object.create(Coordinate).init(vertices[3].x, vertices[3].y)

        this.originalHeight = this.bottomRight.y - this.topRight.y
        this.originalWidth = this.bottomRight.x - this.bottomLeft.x
        
        this.centre = this.calculateCentre(this.topLeft.x, this.topLeft.y, this.bottomRight.x, this.bottomRight.y)
        
        this.key = key
        this.confidence = 0
        
        this.id = this.generateId(this.centre.x, this.centre.y, this.string)

        return this
    },

    generateId: function(x, y, string){
        const secret = '420cookies'
    
        return crypto
        .createHmac('sha256', secret)
        .update(x.toString()+y.toString()+string)
        .digest('hex')
    },

    calculateCentre: function(x1, y1, x2, y2){
        return Object.create(Coordinate).init(Math.floor((x1+x2)/2), Math.floor((y1+y2)/2))  
    },
}

const Coordinate = {
    init: function(x, y){
        this.x = x
        this.y = y

        return this
    }
}

module.exports = WordObject