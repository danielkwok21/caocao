module.exports = {
    port: 8888,
    uploadsDir: './public/uploads/',
    apiResultsDir: './public/apiResults/',
    documentsDir: './public/documents/',
    labelledDir: './public/labelleds/',
    apikeyPath: './private/apikey.json',
    preProcessedDir: './public/preprocessed',
    labellingScriptPath: './python/labelling.py',
    preProcessingScriptPath: './python/preProcessing.py',
}