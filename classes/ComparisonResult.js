const ComparisonResult = {
    init: function(compared, comparing, result, similarity){
        this.compared = compared;
        this.comparing = comparing;
        this.result = result;
        this.similarity = similarity;

        return this
    }
}

module.exports = ComparisonResult