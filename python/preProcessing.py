import ImageProcessing as ip
import Util as u
import numpy as np
import cv2
import sys

ori_image_path = sys.argv[1]
api_img_path = sys.argv[2]
labelled_img_path = sys.argv[3]

ori = cv2.imread(ori_image_path, cv2.IMREAD_COLOR)
img = ori
img = cv2.GaussianBlur(img, (5, 5), 0)

# align image
contourImg, angle, isHorz = ip.getAlignAngle(img)

# aligned image for labelling
img = ip.rotateImage(ori, angle)
cv2.imwrite(labelled_img_path, img)

# v and aligned image for api
h, s, v = cv2.split(img)
img = v
cv2.imwrite(api_img_path, img)

print 'PreVision script done.'