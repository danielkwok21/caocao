import ImageProcessing as ip
import Util as u
import numpy
import cv2
import json
import sys

documentPath = sys.argv[1]
document = json.load(open(documentPath))

ori = cv2.imread(document['imgPath'])
img = ori
wordObjects = document['wordObjects']
red = (0, 0, 255)
lightred = (100, 100, 255)

orange = (0, 162, 255)
lightorange = (168, 203, 255)

green = (0, 100, 0)
lightgreen = (0, 255, 0)

black = (0, 0, 0)
lightblack = (150, 150, 150)

blue = (255, 0, 0)
lightblue = (255, 255, 0)

purple = (255, 0, 150)
lightpurple = (255, 175, 200)

for w in wordObjects:
	centre = (w['centre']['x'], w['centre']['y'])
	topLeft = (w['topLeft']['x'], w['topLeft']['y'])
	bottomRight = (w['bottomRight']['x'], w['bottomRight']['y'])

	# location field marked blue and lightblue
	if w['key']=='found':
		cv2.rectangle(img, topLeft, bottomRight, red, 1)

cv2.imwrite(document['labelledImgPath'], img)

print 'PostVision script done.'