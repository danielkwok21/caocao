const utils = require('../utils')
const path = require('path')
const fs = require('fs')
const config = require('../config')

const Document = {
    init: function(name){
        this.path = path.join(config.documentsDir, 'document_'+name.split('.')[0]+'.json'),
        this.imgPath =  path.join(config.uploadsDir, name),
        this.labelledImgPath = path.join(config.labelledDir, name),
        this.apiResultPath = path.join(config.apiResultsDir, 'apiResult_'+name.split('.')[0]+'.json')
        
        return this
    }
}

module.exports = Document