'use strict'
const http = require('http')
const fs = require('fs')
const os = require('os')
const path = require('path')
const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const config = require('./config')
const utils = require('./utils')
const vision = require('@google-cloud/vision')
const {PythonShell} = require('python-shell')
const multer = require('multer')
const moment = require('moment')

const WordObject = require('./classes/WordObject')
const Document = require('./classes/Document')
const Database = require('./classes/Database')

//sets view engine as the package ejs
app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, './public/views'))
app.use(express.static('public'))
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())


// multer setup
const storage = multer.diskStorage({
    destination:config.uploadsDir,
    filename: function(req, file, cb){
        let fileName = file.fieldname+'_'+Date.now()+path.extname(file.originalname)
        cb(null, fileName)
    }
})
const upload = multer({
    storage: storage,
}).array('image', 10)

app.get('/', (req, res)=>{
    renderFrontEnd(res)
})

app.post('/upload', (req, res) =>{  
    console.log('uploading...')

    upload(req, res, (err)=>{
        fs.readdir(config.uploadsDir, (err, files)=>{
            console.log(files)
            // files.forEach(file=>{
            //     let document = Object.create(Document).init(file)
            //     generateWordObjects(document)
            // })
        })

        console.log('...upload complete!')

        renderFrontEnd(res)
    })
})

app.post('/search', (req, res) =>{  
    let keyword = req.body.keyword
    console.log(keyword)
    console.log('searching with keyword: '+keyword)

    fs.readdir(config.documentsDir, (err, results) => {
        let promises = []
        if (err) throw err;
        
        for (const result of results) {
            let newPromise = new Promise((res, rej)=>{
                fs.readFile(path.join(config.documentsDir, result), (err, data)=>{
                    err? rej(err):res(data)
                })
            })   
            promises.push(newPromise)       
        }

        Promise.all(promises)
        .then(data=>{
            console.log('DONE')
            let documents = data.map(json=>JSON.parse(json))
            console.log('length: ', documents.length)
            searchAndRender(res, keyword, documents)
        })
        .catch(err=>{
            console.error('ERR at search')
            console.error(err)
        })
        
    })
})

app.get('/db', (req, res)=>{
    const db = Object.create(Database).init()
    db.query('DROP TABLE users')
    .then(result=>{
        res.json(result)
    })


    // connection.connect(err=>{
    //     if(err){
    //         console.error('error in connecting sql connection')
    //         return console.log(err)
    //     }

    //     console.log('connected to mysql server')
    // })
    // connection.end(err=>{
    //     if(err){
    //         console.error('error in ending sql connection')
    //         return console.log(err)
    //     }

    //     console.log('connection ended mysql server')
    // })
})

function getUploadedImgNames(){
    return fs.readdirSync(config.uploadsDir)
}

function clearDir(dir){   
    fs.readdirSync(dir, (err, files) => {
        if (err) throw err;
    
        for (const file of files) {
            fs.unlink(path.join(dir, file), err => {
                if (err) throw err;
            });
        }
    });
}

function wait(filename){
    return new Promise((resolve, reject)=>{
        console.log('waiting '+filename+'...')
        setTimeout(() => resolve(filename+' done!'), 1000)
    });
}

function generateWordObjects(document){
    const img = document.imgPath

    return callAPI(document)
    .then(result=>{
        return processAPIResult(result, document)
    })
    .then(document=>{        
        utils.writeToFile(document, document.path)
        return document
    })
    .catch(err=>{
        throw err
    })   
}

function setKeyByString(document, string){
    let match = 0
    document.wordObjects.forEach(wordObject=>{
        let comparisonResult = utils.wordComparison(wordObject.string, string)
        if(comparisonResult.result){
            wordObject.key = 'found'
            wordObject.confidence = comparisonResult.similarityIndex
            match++
        }else{
            wordObject.key = ''
            wordObject.confidence = 1
        }
    })
    return {document: document, match: match}
}

function searchAndRender(res, keyword, documents){
    clearDir(config.labelledDir)
    let pendingPromises = []
    documents.forEach(document=>{
        let result = setKeyByString(document, keyword)
        if(result.match){
            utils.writeToFile(result.document, document.path)
            pendingPromises.push(labellingScript(result.document))
        }
    })

    Promise.all(pendingPromises)    
    .then(imgPaths=>{
        let imgNames = imgPaths.map(imgPath=>path.basename(imgPath))
        renderFrontEnd(res, imgNames)
    })
}

function renderFrontEnd(res, imgNames = []){
    let uploadedImgNames = getUploadedImgNames()
    res.render('index.ejs', {
        uploadedImgNames: uploadedImgNames,
        searchedImgNames: imgNames
    })
}

/**
 * Run python script to draw lines based on data in apiResult.json on img for visual feedback of K-means algorithm in work
 * @param {string} imgPath Path to pre-proccesed image file 
 */
function labellingScript(document){
    const scriptPath = config.labellingScriptPath
    
    // console.log('running '+path.basename(scriptPath)+'...')
    const options = {
        args: [
            document.path
        ]
    }
    
    return new Promise((resolve, reject)=>{
        PythonShell
        .run(scriptPath, options, function(err, results){
            err?reject(err):resolve(results)
        })
        // .on('message', message=>{
        //     console.log('From python: '+message)
        // })
    })
    .then(()=>{
        // console.log(path.basename(scriptPath)+' done')
        return document.labelledImgPath
    })
    .catch(err=>{
        console.log('ERROR IN PYTHON', err)
    })

}

/**
 * Makes request to Google Cloud Vision API to populate apiResult.json
 * @param {string} imagePath Path to pre-processed image file 
 */
function callAPI(document){
    console.log('calling api...')

    //create client
    const client = new vision.ImageAnnotatorClient({
        keyFilename: config.apikeyPath
    })

    console.log('running ocr...')

    //call api
    return client
    .documentTextDetection(document.imgPath)    
    .then(results=>{
        utils.writeToFile(results[0], document.apiResultPath)
        return results[0]
    })
    .then(results=>{
        console.log('...ocr done. Result in '+document.apiResultPath)
        return results
    })
}

/**
 * Populates wordObjects.json by doing addtional work on original apiResult.json
 * @param {string} result Result returned from api call
 */
function processAPIResult(result, document){
    // console.log('processing results...'+document.path)

    return new Promise((resolve, reject)=>{

        //create wordObject based on apiResult
        let wordObjects = []  
        for(let i=1; i<result.textAnnotations.length; i++){
            let object = result.textAnnotations[i]
            let wordObject = 
            Object
            .create(WordObject)
            .init(object.description, object.boundingPoly.vertices)                
            wordObjects.push(wordObject)
        }
        document.wordObjects = wordObjects

        // console.log('...filtering done.')

        resolve(document)
    })
}


function searchWordObjectById(wordObjects, id){
    return wordObjects.find(wordObject=>{
        return wordObject.id===id
    })
}

function isValue(key){
    const x = key.split('_')
    return x[x.length-1]==='value'
}

function isField(key){
    const x = key.split('_')
    return x[x.length-1]==='field'
}

function removeWordObjectById(wordObjects, id){
    let toBeRemoved = searchWordObjectById(wordObjects, id)
    if(toBeRemoved){            
        wordObjects = wordObjects.filter(wordObject=>{
            return wordObject.id!==id
        })
        wordObjects.forEach(wordObject=>{
            wordObject.neighbours = wordObject.neighbours.filter(neighbour=>{
                return neighbour.id!==id
            })
        })
    }

    return wordObjects
}

let port = process.env.PORT || config.port
app.listen(port, ()=>{
    console.log('Connection established')
    console.log('Go to localhost:'+port)
})